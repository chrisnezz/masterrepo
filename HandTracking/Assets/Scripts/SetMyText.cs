﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SetMyText : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI numberText;
   
    public void SetTheText(string n)
    {
        numberText.text = n.ToString();
    }
}
