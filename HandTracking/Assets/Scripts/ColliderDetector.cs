﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ColliderDetector : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI debugText;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        debugText.text = other.gameObject.tag;
    }
}
