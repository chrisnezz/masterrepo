﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotMachine : MonoBehaviour
{
    [SerializeField] GameObject rotationPart;
    [SerializeField] private float RotationSpeed;
    private bool isSpinning = false;

    public void StartSpin()
    {
        if (!isSpinning)
        {
            isSpinning = true;
            StartCoroutine(SpinMe());
        }
    }

    private  IEnumerator SpinMe()
    {
        float t = Random.Range(0.5f, 1.5f);

        while(t > 0)
        {
            rotationPart.transform.Rotate(Vector3.up * (RotationSpeed * Time.deltaTime));
            yield return new WaitForEndOfFrame();
            t -= Time.deltaTime;
        }

        isSpinning = false;
    }
}
