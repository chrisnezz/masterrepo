﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DetectHandPinch : MonoBehaviour
{
    private OVRHand hand;
    private Transform indexPos;
    [SerializeField] private TextMeshProUGUI debugTexting; 
    private LineRenderer _lineRender; 
    
    // Start is called before the first frame update
    void Start()
    {
        indexPos = GetEnumBoneTransform(OVRPlugin.BoneId.Hand_IndexTip);
        _lineRender = GetComponent<LineRenderer>();
        hand = GetComponent<OVRHand>();
    }

    void Update()
    {
        Vector3 fwd = indexPos.transform.TransformDirection(Vector3.right);

        RaycastHit hit;

        if (Physics.Raycast(indexPos.transform.position, fwd, out hit, 10))
            Debug.DrawRay(indexPos.transform.position, fwd * 10, Color.red);
        Debug.Log(hit.collider.name);

        if (hit.collider.name != "Cube")
        {
            _lineRender.SetPosition(0, indexPos.transform.position);
            _lineRender.SetPosition(1, hit.transform.position);
        }
        else
        {
            _lineRender.SetPosition(0, indexPos.transform.position);
            _lineRender.SetPosition(1, indexPos.transform.position + (fwd * 3));
        }
    }

    // Update is called once per frame
    /*void Update()
    {
        if (hand == null)
        {
            return;
        }
        
        bool isIndexFingerPinching = hand.GetFingerIsPinching(OVRHand.HandFinger.Index);
        if (isIndexFingerPinching)
        {
            debugTexting.text = indexPos.transform.position.ToString();
            
        }
        else
        {
            debugTexting.text = indexPos.transform.rotation.ToString();
        }
    }*/
    
    Transform GetEnumBoneTransform(OVRPlugin.BoneId bones)
    {
        Transform trf;
        GameObject enumObj = GameObject.Find(bones.ToString());
        trf = enumObj.transform;
        return trf;
    }
}
