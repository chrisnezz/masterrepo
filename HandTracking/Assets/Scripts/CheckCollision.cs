﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = System.Random;

public class CheckCollision : MonoBehaviour
{
    [SerializeField] private float bigWeight;
    [SerializeField] private float mediumWeigh;
    [SerializeField] private float smallWeight;
    
    [SerializeField] private GameObject leftHand;
    [SerializeField] private GameObject rightHand;
    
    [SerializeField] private TextMeshProUGUI checkText;
    [SerializeField] private TextMeshProUGUI leftScaleText;
    [SerializeField] private TextMeshProUGUI taskDescription;
    
    [SerializeField] private AudioClip wrongSound;
    [SerializeField] private AudioClip correctSound;
    [SerializeField] private AudioClip popSound;
    [SerializeField] private AudioSource completionSound;
    [SerializeField] private AudioSource objectSound;
    
    [SerializeField] private List<Image> circles;
    [SerializeField] private List<Image> checks;
    [SerializeField] private List<Image> wrongs;
    [SerializeField] private List<Transform> weightsStartTransform = new List<Transform>();
    [SerializeField] private List<Vector3> weightVectors = new List<Vector3>();

    [SerializeField] private CheckWeight _checkWeight;
    
    private List<string> twoKGList = new List<string>();
    private List<string> oneKGList = new List<string>();
    private List<string> fiveGList = new List<string>();
    private float kilosOnWeight = 0.0f;
    private float[] requiredWeights = new []{4.5f, 5.0f, 5.5f};
    private float timer;
    private int count = 0;
    private int completionCount = 3;
    private int wrongSubmissionsCount = 0;
    private bool balanced = false;
    private bool finished = false;
    private bool started = false;
    private OVRSkeleton _LovrSkeleton;
    private OVRSkeleton _RovrSkeleton;
    
    public static CheckCollision checkCollision;

    void Start()
    {
        if (checkCollision == null)
        {
            checkCollision = this;
        }

        foreach (Image im in checks)
        {
            im.enabled = false;
        }
        
        foreach (Image im in wrongs)
        {
            im.enabled = false;
        }

        taskDescription.text = "Place weights on the scales to balance the weight.\n Push the button in front of you to get started!";
        
        _LovrSkeleton = leftHand.GetComponent<OVRSkeleton>();
        
        _RovrSkeleton = rightHand.GetComponent<OVRSkeleton>();
        
        _LovrSkeleton.SetTriggerTrue();
        _RovrSkeleton.SetTriggerTrue();

        foreach (Transform t in weightsStartTransform)
        {
            weightVectors.Add(t.position);
            t.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (started && !finished)
        {
            timer += Time.deltaTime; 
        }
    }

    private void SetText()
    {
        leftScaleText.text = kilosOnWeight + " KG";
    }
    
    public void CheckBalance()
    {
        if(finished)
        {
            SceneManager.LoadScene("ScenarioChoser");
        }
        else
        {
            if (!started)
            {
                objectSound.PlayOneShot(popSound);

                foreach (Image im in circles)
                {
                    im.enabled = true;
                }

                started = true;
                
                foreach (Transform t in weightsStartTransform)
                {
                    t.gameObject.SetActive(true);
                }

                taskDescription.text = "Place " + requiredWeights[count].ToString("F1") + " KG on each weight.";
            }
            else
            {
                CheckForBalance();
            }
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "2KG")
        {
            twoKGList.Add(other.gameObject.tag);
            kilosOnWeight += bigWeight;
        } else if (other.gameObject.tag == "1KG")
        {
            oneKGList.Add(other.gameObject.tag);
            kilosOnWeight += mediumWeigh;
        } else if (other.gameObject.tag == "500G")
        {
            fiveGList.Add(other.gameObject.tag);
            kilosOnWeight += smallWeight;
        }

        SetText();
        
        CheckIfIsBalanced();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "2KG")
        {
            twoKGList.Remove(other.gameObject.tag);
            kilosOnWeight -= bigWeight;
        } else if (other.gameObject.tag == "1KG")
        {
            oneKGList.Remove(other.gameObject.tag);
            kilosOnWeight -= mediumWeigh;
        } else if (other.gameObject.tag == "500G")
        {
            fiveGList.Remove(other.gameObject.tag);
            kilosOnWeight -= smallWeight;
        }

        SetText();
        
        CheckIfIsBalanced();
    }

    public void CheckIfIsBalanced()
    {
        if (kilosOnWeight == requiredWeights[count] && kilosOnWeight == _checkWeight.kilosOnWeight)
        {
            balanced = true;
        }
        else
        {
            balanced = false;
        }
    }

    private void CheckForBalance()
    {
        if (balanced)
        {
            if (count < completionCount)
            {
                circles[count].color = Color.green;
                checks[count].enabled = true;
                completionSound.PlayOneShot(correctSound);
                objectSound.PlayOneShot(popSound);

                count++;

                int i = 0;

                foreach (Transform t in weightsStartTransform)
                {
                    t.position = weightVectors[i];
                    t.rotation = new Quaternion(0f, 0f, 0f, 0f);
                    i++;
                }

                balanced = false;

                if (count < completionCount)
                {
                    taskDescription.text = "Place " + requiredWeights[count].ToString("F1") + " KG on each weight.";
                }
                else
                {
                    _LovrSkeleton.SetTriggerFalse();
                    _RovrSkeleton.SetTriggerFalse();

                    taskDescription.text = "Done! Press the button in front of you to return";

                    finished = true;

                    SendAnalytics();
                }
            }
        }
        else
        {
            completionSound.PlayOneShot(wrongSound);
            StartCoroutine(WrongSubmit(count));
            wrongSubmissionsCount++;
        }
    }

    private IEnumerator WrongSubmit(int index)
    {
        wrongs[index].enabled = true;
        circles[index].color = Color.red;
        yield return new WaitForSeconds(0.5f);
        wrongs[index].enabled = false;
        circles[index].color = Color.white;
    }
    
    private void SendAnalytics()
        {
            Analytics.CustomEvent("GraspAndRelease_" + SystemInfo.deviceUniqueIdentifier, new Dictionary<string, object>
            {
                { "UniqueID", PlayerPrefs.GetInt("UniqueID")},
                { "CompletionTime", timer},
                { "IncorrectSubmissions", wrongSubmissionsCount },
                { "AmountOfPinches", SingletonScript.singletonScript.amountOfPinches },
                { "AoPHitObject",  SingletonScript.singletonScript.amountOfCorrectPinches }
            });
        }
}
