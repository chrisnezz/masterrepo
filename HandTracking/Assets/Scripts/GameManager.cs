﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private TextMeshProUGUI allTimerText;
    [SerializeField] private TextMeshProUGUI headerText;
    private float timer = 0;
    public static GameManager gameManger;
    [SerializeField] private GameObject[] allCorrect;
    [SerializeField] private GameObject allParent;
    [SerializeField] private GameObject returnButton;
    [SerializeField] private AudioClip wrongSound;
    [SerializeField] private AudioClip correctSound;
    [SerializeField] private AudioSource audioSource; 


    int count = 0;
    private bool started = false;
    private int _amountOfPinches;

    // Start is called before the first frame update
    void Start()
    {
        if(gameManger == null)
        {
            gameManger = this;
        }

        SetAllTexts();

        allTimerText.text = "";
        timerText.text = "";
    }

    private void SetAllTexts()
    {
        int i = 1;
        foreach (GameObject el in allCorrect) {
            el.GetComponent<SetMyText>().SetTheText(i.ToString());
            el.GetComponent<ButtonListener>().actionEvent.AddListener(SetTimeTextWrongNumberCorrectColor);
            i++;
        }

        allParent.SetActive(false);
    }

    public void DisableObject()
    {
        allCorrect[count].gameObject.SetActive(false);
    }

    void Update()
    {
        if(count >= 12 || !started ) {
            return;
        }

        timer += Time.deltaTime;
        timerText.text = timer.ToString("F2");
    }

    public void StartTimer()
    {
        if (!started)
        {
            headerText.text = "Started!";
            started = true;
            SetNewTarget();
            _amountOfPinches = 0;
        }
    }

    public void SetTimeTextCorrect()
    {
        if (count >= 12 || !started)
        {
            return;
        }

        audioSource.PlayOneShot(correctSound);
        
        count += 1;
        SetNewTarget();
        allTimerText.text += "C" + timer.ToString("F3") + "    -    ";

        if (count == 12)
        {
            headerText.text = "Finished!";
            returnButton.SetActive(true);
            allParent.SetActive(false);
            string filePath = Application.persistentDataPath + "/results.txt";
            string temp = timer.ToString("F3") + "    -    ";

            TextWriter tw = new StreamWriter(filePath, true);
            tw.WriteLine("NEW TEST:    " + PlayerPrefs.GetString("SCENENAME"));
            tw.WriteLine("Individual clicks:    " + allTimerText.text);
            tw.WriteLine("Finish Time: " + temp);
            tw.WriteLine("Amount of pinches:    " + _amountOfPinches.ToString());
            tw.WriteLine("______________________________________________________");
            tw.Close();

        }
    }

    private void SetNewTarget()
    {
        if (count < 12)
        {
            ButtonListener bl = allCorrect[count].GetComponent<ButtonListener>();
            bl.actionEvent.RemoveAllListeners();
            //UnityEventTools.RemovePersistentListener(bl.actionEvent,1);
            bl.actionEvent.AddListener(DisableObject);
            bl.actionEvent.AddListener(SetTimeTextCorrect);
        }
    }

    public void SetTimeTextWrong()
    {
        if (count >= 12 || !started)
        {
            return;
        }

        audioSource.PlayOneShot(wrongSound);

        allTimerText.text += "W" + timer.ToString("F3") + "    -    ";
    }

    public void SetTimeTextWrongNumberCorrectColor()
    {
        if (count >= 12 || !started)
        {
            return;
        }

        audioSource.PlayOneShot(wrongSound);

        allTimerText.text += "N" + timer.ToString("F3") + "    -    ";
    }

    public void AddPinch()
    {
        _amountOfPinches += 1;
    }
}
