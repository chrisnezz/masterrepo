﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

public class TextOnSlider : MonoBehaviour
{

    [SerializeField] Text _sliderValueText;
    [SerializeField] Light _light;
    [SerializeField] GameObject _slider;

    void Start()
    {
        _sliderValueText.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        _sliderValueText.text = Mathf.Clamp((_slider.transform.localPosition.x * 50),0,100).ToString("F1");

        if (_light != null)
        {
            _light.intensity = Mathf.Clamp((_slider.transform.localPosition.x * 5), 0, 10);
        }
    }
}
