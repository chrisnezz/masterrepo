﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameButton : MonoBehaviour
{
    [SerializeField] private GameObject allPicks;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Button")
        {
            GameManager.gameManger.StartTimer();
        }
    }

    public void StartGame()
    {
        GameManager.gameManger.StartTimer();
    }

    public void ShowAll()
    {
        allPicks.SetActive(true);
    }
}
