﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CompletionSubmit : MonoBehaviour
{
    [SerializeField] private Transform _interactableButtonsParent;
    [SerializeField] private Transform _replicationObjectsParent;

    [SerializeField] private TextMeshProUGUI _feedbackText;
    
    [SerializeField] private Material whiteMaterial;
    [SerializeField] private Material blackMaterial;
    
    [SerializeField] private GameObject referenceObject;
    
    [SerializeField] private List<Image> checkMarks = new List<Image>();
    [SerializeField] private List<Image> wrongMarks = new List<Image>();
    [SerializeField] private List<Image> circles = new List<Image>();

    [SerializeField] private AudioClip wrongSound;
    [SerializeField] private AudioClip correctSound;
    [SerializeField] private AudioClip popSound;
    [SerializeField] private AudioSource completionSound;
    [SerializeField] private AudioSource referenceSound;
    
    public List<Transform> _interactableButtons;
    public List<Transform> _replicationButtons;

    private bool _isPressed = false;
    private bool started = false;
    private bool finished = false;
    private int tileMatchCount;
    private float _timing;
    private List<int> secondSequence = new List<int>{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24};
    private List<int> thirdSequence = new List<int>{0, 2, 4, 5, 7, 9, 11, 13, 16, 18, 20, 22, 24};
    private int completionCount;
    private int wrongSubmissionsCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Image im in checkMarks)
        {
            im.enabled = false;
        }
        
        foreach (Image im in wrongMarks)
        {
            im.enabled = false;
        }
        
        foreach (Transform ibp in _interactableButtonsParent)
        {
            _interactableButtons.Add(ibp);
        }

        foreach(Transform rop in _replicationObjectsParent)
        {
            _replicationButtons.Add(rop);
        }
        
        referenceObject.SetActive(false);
    }

    void Update()
    {
        if (started && !finished)
        {
            _timing += Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Button")
        {
            if (!started)
            {
                referenceObject.SetActive(true);
                _feedbackText.text = "Replicate the figures on your left by pushing the buttons on the table.";
                started = true;
                referenceSound.PlayOneShot(popSound);
            }
            else if (completionCount < 3)
            {
                _isPressed = true;
                SubmitCheck();
            }
            else
            {
                SceneManager.LoadScene("ScenarioChoser");
            }
        }
    }

    private void SubmitCheck()
    {
        if(started && _isPressed)
        {
            for(int i = 0; i < _interactableButtons.Count; i++)
            {
                if(_interactableButtons[i].GetComponentInChildren<MeshRenderer>().sharedMaterial == _replicationButtons[i].GetComponent<MeshRenderer>().sharedMaterial)
                {
                    tileMatchCount++;
                }
            }

            if(tileMatchCount == _interactableButtons.Count)
            {
                completionSound.PlayOneShot(correctSound);
                referenceSound.PlayOneShot(popSound);
                
                if (completionCount == 0)
                {
                    circles[completionCount].color = Color.green;
                    checkMarks[completionCount].enabled = true;
                    completionCount += 1;
                    
                    for(int i = 0; i < _interactableButtons.Count; i++)
                    {
                        _interactableButtons[i].GetComponentInChildren<MeshRenderer>().material = whiteMaterial;
                        
                        if (i == secondSequence[0])
                        {
                            _replicationButtons[i].GetComponent<MeshRenderer>().material = whiteMaterial;
                            secondSequence.RemoveAt(0);
                        }
                        else
                        {
                            _replicationButtons[i].GetComponent<MeshRenderer>().material = blackMaterial;
                        }
                    }

                    tileMatchCount = 0;
                } else if (completionCount == 1)
                {
                    circles[completionCount].color = Color.green;
                    checkMarks[completionCount].enabled = true;
                    completionCount += 1;
                    
                    for(int i = 0; i < _interactableButtons.Count; i++)
                    {
                        _interactableButtons[i].GetComponentInChildren<MeshRenderer>().material = whiteMaterial;
                        
                        if (i == thirdSequence[0])
                        {
                            _replicationButtons[i].GetComponent<MeshRenderer>().material = whiteMaterial;
                            thirdSequence.RemoveAt(0);
                        }
                        else
                        {
                            _replicationButtons[i].GetComponent<MeshRenderer>().material = blackMaterial;
                        }
                    }

                    tileMatchCount = 0;
                }
                else
                {
                    referenceObject.SetActive(false);
                    circles[completionCount].color = Color.green;
                    checkMarks[completionCount].enabled = true;
                    _feedbackText.text = "Done! Press the button on your right to return";
                    SendAnalytics();
                    tileMatchCount = 0;
                    completionCount += 1;
                    finished = true;
                }
            }
            else
            {
                completionSound.PlayOneShot(wrongSound);
                StartCoroutine(WrongSubmit(completionCount));
                wrongSubmissionsCount++;
                tileMatchCount = 0;
            }
        }
        _isPressed = false;
    }

    private IEnumerator WrongSubmit(int index)
    {
        wrongMarks[index].enabled = true;
        circles[index].color = Color.red;
        yield return new WaitForSeconds(0.5f);
        wrongMarks[index].enabled = false;
        circles[index].color = Color.white;
    }

    private void SendAnalytics()
    {
        Analytics.CustomEvent("ButtonGame_" + SystemInfo.deviceUniqueIdentifier, new Dictionary<string, object>
        {
            { "UniqueID", PlayerPrefs.GetInt("UniqueID")},
            { "CompletionTime", _timing},
            { "IncorrectSubmissions", wrongSubmissionsCount },
            { "AmountOfButtonPresses", SingletonScript.singletonScript.totalButtonsPressed }
        });
    }
}
