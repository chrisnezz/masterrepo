﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCurveScript : MonoBehaviour
{
    [SerializeField] private float _noCurve;
    [SerializeField] private float _someCurve;
    [SerializeField] private float _moreCurve;
    [SerializeField] private float _mostCurve;
    [SerializeField] private float _snappingOn;
    [SerializeField] private float _snappingOff;

    [SerializeField] private float _FarOn;
    [SerializeField] private float _FarOff;

    public void NoCurve()
    {
        SingletonScript.singletonScript.curve = _noCurve;
    }

    public void SomeCurve()
    {
        SingletonScript.singletonScript.curve = _someCurve;
    }

    public void MoreCurve()
    {
        SingletonScript.singletonScript.curve = _moreCurve;
    }

    public void MostCurve()
    {
        SingletonScript.singletonScript.curve = _mostCurve;
    }

    public void SetSnappingOn()
    {
        SingletonScript.singletonScript.snapping = _snappingOn;
        SingletonScript.singletonScript.farfetch = _FarOn;
    }

    public void SetSnappingOff()
    {
        SingletonScript.singletonScript.snapping = _snappingOff;
        SingletonScript.singletonScript.farfetch = _FarOff;
    }

    public void SetPrefab(string s)
    {
        PlayerPrefs.SetString("SCENENAME", s);
    }
}
