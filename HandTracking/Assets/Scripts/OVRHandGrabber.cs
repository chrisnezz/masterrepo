﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OculusSampleFramework;
using TMPro;

public class OVRHandGrabber : OVRGrabber
{
    private OVRHand.Hand hand;
    public float pinchTreshold;
    private bool newPinch = true;
    private bool newCorrectPinch = true;

    protected override void Start()
    {
        base.Start();
        GetComponent<OVRHand.Hand>();
    }

    public override void Update()
    {
        base.Update();
        CheckIndexPinch();
    }

    void CheckIndexPinch()
    {
        float pinchStrength = GetComponent<OVRHand>().GetFingerPinchStrength(OVRHand.HandFinger.Index);
        bool isPinching = pinchStrength > pinchTreshold;

        if (isPinching && newPinch)
        {
            SingletonScript.singletonScript.amountOfPinches++;
            newPinch = false;
        } else if (!isPinching)
        {
            newCorrectPinch = true;
            newPinch = true;
        }
        
        if (!m_grabbedObj && isPinching && m_grabCandidates.Count > 0)
        {
            GrabBegin();

            if (newCorrectPinch)
            {
                SingletonScript.singletonScript.amountOfCorrectPinches++;   
                newCorrectPinch = false;
            }
        }
        else
        {
            GrabEnd();
        }
    }
}
