﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HandDebuggerScript : MonoBehaviour
{
    private OVRHand hand;
    private bool newPinch = true;

    void Start()
    {
        hand = GetComponent<OVRHand>();
    }

    void Update()
    {
        if (hand == null)
        {
            return;
        }
        
        bool isIndexFingerPinching = hand.GetFingerIsPinching(OVRHand.HandFinger.Index);

        if (isIndexFingerPinching && newPinch)
        {
            GameManager.gameManger.AddPinch();
            newPinch = false;   
        }

        if (!isIndexFingerPinching)
        {
            newPinch = true;
        }
    }
}
