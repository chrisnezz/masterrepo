﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationConstains : MonoBehaviour
{
    Rigidbody rb;
    bool doing = false;
    private Quaternion startRotation;
    private Vector3 startPos;
    [SerializeField] private SlotMachine slotMachine;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        startRotation = transform.rotation;
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(rb.rotation.x >= 0.6 && !doing)
        {
            doing = true;
            
            StartCoroutine(ResetRotatation());
            slotMachine.StartSpin();
        }
    }

    private IEnumerator ResetRotatation()
    {
        yield return new WaitForSeconds(0.2f);
        
        while (transform.localRotation.x > 0.05)
        {
            transform.Rotate(new Vector3(1f, 0f, 0f), 20f * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        
        transform.rotation = startRotation;
        transform.position = startPos;

        yield return new WaitForSeconds(0.1f);
        doing = false;
    }
}
