﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CheckWeight : MonoBehaviour
{
    [HideInInspector] public float kilosOnWeight = 0.0f;
    [SerializeField] private TextMeshProUGUI rightScaleText;

    private void Start()
    {
        SetText();
    }

    private void SetText()
    {
        rightScaleText.text = kilosOnWeight + " KG";
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "2KG")
        {
            kilosOnWeight += 2.0f;
        } else if (other.gameObject.tag == "1KG")
        {
            kilosOnWeight += 1.0f;
        } else if (other.gameObject.tag == "500G")
        {
            kilosOnWeight += 0.5f;
        }
        
        SetText();
        
        CheckCollision.checkCollision.CheckIfIsBalanced();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "2KG")
        {
            kilosOnWeight -= 2.0f;
        } else if (other.gameObject.tag == "1KG")
        {
            kilosOnWeight -= 1.0f;
        } else if (other.gameObject.tag == "500G")
        {
            kilosOnWeight -= 0.5f;
        }

        SetText();
        
        CheckCollision.checkCollision.CheckIfIsBalanced();
    }
}
