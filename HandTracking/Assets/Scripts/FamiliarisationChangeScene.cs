﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FamiliarisationChangeScene : MonoBehaviour
{
    private float t = 1.0f;
    [SerializeField] private string sceneName;
    
    private void Update()
    {
        if (t >= 0)
        {
            t -= Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Button" && t < 0)
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
