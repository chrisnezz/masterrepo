﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonScript : MonoBehaviour
{
    public float curve = 10f;
    public float snapping = 10f;
    public float farfetch = 5f;
    public static SingletonScript singletonScript;

    public int amountOfPinches = 0;
    public int amountOfCorrectPinches = 0;
    public int totalButtonsPressed = 0;

    void Start()
    {
        if (singletonScript == null)
        {
            singletonScript = this;   
        }
        
        DontDestroyOnLoad(gameObject);
    }
}
