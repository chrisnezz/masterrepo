﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangeText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI number;
   
    public void StartEnum()
    {
        GameManager.gameManger.SetTimeTextWrongNumberCorrectColor();
        //StartCoroutine(ChangeNumberBriefly());
    }

    private IEnumerator ChangeNumberBriefly()
    {
        string s = number.text;
        number.text = "NO";
        GameManager.gameManger.SetTimeTextWrongNumberCorrectColor();
        yield return new WaitForSeconds(0.5f);
        number.text = s;
    }
}
