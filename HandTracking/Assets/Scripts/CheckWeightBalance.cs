﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CheckWeightBalance : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI zRotatePos;
    [SerializeField] private TextMeshProUGUI checkText;
    private int zPos;
    private bool balanceIsRight;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        zPos = (int) gameObject.GetComponent<Transform>().localRotation.eulerAngles.z;
        zRotatePos.text = zPos.ToString("F2");

        if (zPos == 359)
        {
            balanceIsRight = true;
        }
        else
        {
            balanceIsRight = false;
            checkText.text = "NO";
        }

        /*if (balanceIsRight)
        {
            Invoke("CheckBalance",5.0f);
        }*/
    }

    public void CheckBalance()
    {
        if (balanceIsRight)
        {
            checkText.text = "Yes";   
        }
        else
        {
            checkText.text = "No";
        }
    }
    
    IEnumerator WaitToCheckBalance()
    {
        yield return new WaitForSeconds(2f);
        checkText.text = "Yes";
    }
}
