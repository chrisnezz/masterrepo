﻿using System;
using System.Collections;
using OculusSampleFramework;
using UnityEngine;
using UnityEngine.Events;

public class ButtonListener : MonoBehaviour
{
    public UnityEvent proximityEvent;
    public UnityEvent contactEvent;
    public UnityEvent actionEvent;
    public UnityEvent defaultEvent;
    private MeshRenderer _meshRenderer;

    void Start()
    {
        _meshRenderer = gameObject.GetComponent<MeshRenderer>();
        GetComponent<ButtonController>().InteractableStateChanged.AddListener(InitiateEvent);
    }


    void InitiateEvent(InteractableStateArgs state)
    {
        if (state.NewInteractableState == InteractableState.ProximityState)
            proximityEvent.Invoke();
        else if (state.NewInteractableState == InteractableState.ContactState)
            contactEvent.Invoke();
        else if (state.NewInteractableState == InteractableState.ActionState)
            actionEvent.Invoke();
        else
            defaultEvent.Invoke();
    }

    public void SetTimerTextCorrect()
    {
        StartCoroutine(DisableAndEnableSnapping());
        GameManager.gameManger.SetTimeTextCorrect();
    }

    public void SetTimerTextWrong()
    {
        GameManager.gameManger.SetTimeTextWrong();
    }

    public void SetScaleUp()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
    }

    public void SetScaleDown()
    {
        transform.localScale = new Vector3(.8f, .8f, .8f);
    }
    
    public void DisableAndEnable()
    {
        StartCoroutine(EnableAfterSeconds());
    }

    IEnumerator DisableAndEnableSnapping()
    {
        float s = SingletonScript.singletonScript.snapping;
        float f = SingletonScript.singletonScript.farfetch;
        SingletonScript.singletonScript.snapping = 1f;
        SingletonScript.singletonScript.farfetch = 1f;
        yield return new WaitForSeconds(0.2f);
        SingletonScript.singletonScript.snapping = s;
        SingletonScript.singletonScript.farfetch = f;
    }
    
    IEnumerator EnableAfterSeconds()
    {
        _meshRenderer.enabled = false;
        yield return new WaitForSeconds(2f);
        _meshRenderer.enabled = true;
    }
}
