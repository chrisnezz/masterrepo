﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public void SceneToLoad(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    private void Start()
    {
        string filePath = Application.persistentDataPath + "/results.txt";

        if (File.Exists(filePath))
        {
            Debug.Log(filePath + " already exists.");
        }
        else
        {
            File.CreateText(filePath);
        }
    }
}
