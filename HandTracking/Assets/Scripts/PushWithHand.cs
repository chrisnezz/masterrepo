﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushWithHand : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    private Vector3 startPosition;
    private bool triggered = false;

    private void Start()
    {
        StartCoroutine(WaitSetStartPos());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!triggered)
        {
            StartCoroutine(OnlyTriggerOnce());
        }
    }
    
    private IEnumerator WaitSetStartPos()
    {
        yield return new WaitForSeconds(0.5f);
        startPosition = transform.position;
    }
    
    private IEnumerator OnlyTriggerOnce()
    {
        triggered = true;
        rb.AddForce(-Vector3.up, ForceMode.Impulse);
        yield return new WaitForSeconds(0.6f);
        transform.position = startPosition;
        triggered = false;
    }
}
