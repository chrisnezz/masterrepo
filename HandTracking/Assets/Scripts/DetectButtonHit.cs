﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectButtonHit : MonoBehaviour
{
    [SerializeField] Light _light;

    [Header("Colors")]
    [SerializeField] Color _startColor;
    [SerializeField] Color _clickColor;
    [SerializeField] Color _sometimesColor;


    void Start()
    {
        _light.color = _startColor;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Hit: " + other.name);
        if (other.gameObject.tag == "Button")
        {

            int i = Random.Range(0, 5);

            if (_light.color == _startColor || _light.color == _sometimesColor)
            {
                _light.color = _clickColor;
            }
            else
            {
                _light.color = _sometimesColor;
            }
        }
    }
}
