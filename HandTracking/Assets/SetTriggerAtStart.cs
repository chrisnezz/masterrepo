﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTriggerAtStart : MonoBehaviour
{
    [SerializeField] private List<Collider> buttonList = new List<Collider>();
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitBeforeTrigger());
    }

    private IEnumerator WaitBeforeTrigger()
    {
        yield return new WaitForSeconds(0.5f);
        foreach (Collider col in buttonList)
        {
            col.isTrigger = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
