﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMatOnCollision : MonoBehaviour
{
    [SerializeField] Material startMat;
    [SerializeField] Material changeMat;
    [SerializeField] private float waitTimer;
    float t = 0;

    private void Update()
    {
        if (t > 0)
        {
            t -= Time.deltaTime;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        MeshRenderer mr = collision.gameObject.GetComponent<MeshRenderer>();

        if (t <= 0)
        {
            SingletonScript.singletonScript.totalButtonsPressed++;

            if (mr.material.name == "WhiteMat (Instance)")
            {
                mr.material = changeMat;
                t = waitTimer;
            }

            else if (mr.material.name == "BlackMat (Instance)")
            {
                mr.material = startMat;
                t = waitTimer;
            }
        }
    }
}
