﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandColliderManager : MonoBehaviour
{
    [SerializeField] private GameObject leftHand;
    [SerializeField] private GameObject rightHand;
    
    private OVRSkeleton _LovrSkeleton;
    private OVRSkeleton _RovrSkeleton;
    
    // Start is called before the first frame update
    void Start()
    {
        _LovrSkeleton = leftHand.GetComponent<OVRSkeleton>();
        _RovrSkeleton = rightHand.GetComponent<OVRSkeleton>();
        
        _LovrSkeleton.SetTriggerTrue();
        _RovrSkeleton.SetTriggerTrue();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
