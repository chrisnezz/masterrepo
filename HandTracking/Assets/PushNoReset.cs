﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushNoReset : MonoBehaviour
{
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        rb.AddForce(-Vector3.up, ForceMode.Impulse);
    }
}