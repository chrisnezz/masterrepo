﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DebugTime : MonoBehaviour
{
    [SerializeField] private float _timer;
    [SerializeField] TextMeshProUGUI _timerText;

    public bool _isGameStarted = false;

    void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {
        if(_isGameStarted)
        {
            _timer += Time.deltaTime;
            _timerText.text = _timer.ToString("F2");
        }
    }
}
