﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = System.Random;

public class UniqueIDRoller : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI idText;
    
    void Start()
    {
        if (!PlayerPrefs.HasKey("UniqueID"))
        {
            PlayerPrefs.SetInt("UniqueID", UnityEngine.Random.Range(0,10000));
            idText.text = "ID: #" + PlayerPrefs.GetInt("UniqueID").ToString();
        }
        else
        {
            idText.text = "ID: #" + PlayerPrefs.GetInt("UniqueID").ToString();
        }
    }
}
