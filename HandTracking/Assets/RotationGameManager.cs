﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RotationGameManager : MonoBehaviour
{
    public static RotationGameManager rotGM;
    [SerializeField] private GameObject _masterObj;
    [SerializeField] private GameObject _manipulateObj;
    [SerializeField] private float rotationTime;
    [SerializeField] private float amountToComplete;
    [SerializeField] private float rotationThreshold;

    [SerializeField] private TextMeshProUGUI feedbackText;
    [SerializeField] private TextMeshProUGUI pinches;
    [SerializeField] private Quaternion rotationOne;
    [SerializeField] private Quaternion rotationTwo;
    [SerializeField] private Quaternion rotationThree;
    [SerializeField] private GameObject leftHand;
    [SerializeField] private GameObject rightHand;
    [SerializeField] private List<Image> checkMarks = new List<Image>();
    [SerializeField] private List<Image> wrongMarks = new List<Image>();
    [SerializeField] private List<Image> circles = new List<Image>();
    [SerializeField] private AudioClip wrongSound;
    [SerializeField] private AudioClip correctSound;
    [SerializeField] private AudioClip popSound;
    [SerializeField] private AudioSource completionSound;
    [SerializeField] private AudioSource objectSound;

    private OVRSkeleton _LovrSkeleton;
    private OVRSkeleton _RovrSkeleton;
    private Vector3 startPosition;

    bool started = false;
    bool finished = false;
    int count = 0;
    float time = 0;
    float clickTimer = 0;

    private int wrongSubmissionsCount = 0;

    void Start()
    {
        startPosition = _manipulateObj.transform.position;

        _LovrSkeleton = leftHand.GetComponent<OVRSkeleton>();
        _RovrSkeleton = rightHand.GetComponent<OVRSkeleton>();
        
        _LovrSkeleton.SetTriggerTrue();
        _RovrSkeleton.SetTriggerTrue();

        foreach (Image im in checkMarks)
        {
            im.enabled = false;
        }
        
        foreach (Image im in wrongMarks)
        {
            im.enabled = false;
        }
        
        if(rotGM == null)
        {
            rotGM = this;
        }
    }
    private void Update()
    {
        if (started)
        {
            time += Time.deltaTime;
        }

        if (clickTimer >= 0)
        {
            clickTimer -= Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            _manipulateObj.transform.rotation = rotationOne;
        }
        
        
        if (Input.GetKeyDown(KeyCode.S))
        {
            _manipulateObj.transform.rotation = rotationTwo;
        }
        
        
        if (Input.GetKeyDown(KeyCode.D))
        {
            _manipulateObj.transform.rotation = rotationThree;
        }
    }

    public void StartRotation()
    {
        if (clickTimer <= 0)
        {
            clickTimer = 1f;

            if (!started && !finished)
            {
                objectSound.PlayOneShot(popSound);
                StartCoroutine(RotateRandom());
                _manipulateObj.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
                _manipulateObj.transform.position = startPosition;
                started = true;
                SetText();
            } else if (finished)
            {
                SceneManager.LoadScene("ScenarioChoser");
            } else
            {
                CheckRotation();
            }
        }
    }

    private void SetText()
    {
        if (count == 0)
        {
            feedbackText.text = "Rotate the green cube to match the blue cube.";
        }
        else if (count == 1)
        {
            circles[count - 1].color = Color.green;
            checkMarks[count - 1].enabled = true;
        }
        else if (count == 2)
        {
            circles[count-1].color = Color.green;
            checkMarks[count - 1].enabled = true;
        }
        else if (count == 3)
        {
            circles[count-1].color = Color.green;
            checkMarks[count - 1].enabled = true;
            started = false;
        }

    }

    private void CheckRotation()
    {
        float a = Mathf.Max(_masterObj.transform.rotation.eulerAngles.magnitude, _manipulateObj.transform.rotation.eulerAngles.magnitude);
        float b = Mathf.Min(_masterObj.transform.rotation.eulerAngles.magnitude, _manipulateObj.transform.rotation.eulerAngles.magnitude);

        Debug.Log(a - b);

        if(a - b <= rotationThreshold)
        {
            completionSound.PlayOneShot(correctSound);
            count += 1;
            SetText();
            _manipulateObj.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            _masterObj.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
            _manipulateObj.transform.position = startPosition;
            StartCoroutine(WaitThenRotate());
            Debug.Log("Correct");

        }
        else
        {
            StartCoroutine(WrongSubmit(count));
            completionSound.PlayOneShot(wrongSound);
            wrongSubmissionsCount++;
            Debug.Log("Wrong");
        }
    }
    
    private IEnumerator WrongSubmit(int index)
    {
        wrongMarks[index].enabled = true;
        circles[index].color = Color.red;
        yield return new WaitForSeconds(0.5f);
        wrongMarks[index].enabled = false;
        circles[index].color = Color.white;
    }

    private IEnumerator WaitThenRotate()
    {
        yield return new WaitForSeconds(1f);
        objectSound.PlayOneShot(popSound);
        StartCoroutine(RotateRandom());
    }

    private IEnumerator RotateRandom()
    {
        if (count < amountToComplete)
        {
            Quaternion temp = new Quaternion();
            
            if (count == 0)
            {
                temp = rotationOne;
            } else if (count == 1)
            {
                temp = rotationTwo;
            }
            else
            {
                temp = rotationThree;
            }

            float t = 0;

            while (t < rotationTime)
            {
                _masterObj.transform.rotation = Quaternion.Lerp(_masterObj.transform.rotation, temp, t / rotationTime);
                yield return new WaitForEndOfFrame();
                t += Time.deltaTime;
            }
        }

        else
        {
            finished = true;
            
            feedbackText.text = "Done! Press the button on your right to return";
            
            _LovrSkeleton.SetTriggerFalse();
            _RovrSkeleton.SetTriggerFalse();
            
            SendAnalytics();
        }
    }

    private void SendAnalytics()
    {
        Analytics.CustomEvent("RotationGame_" + SystemInfo.deviceUniqueIdentifier, new Dictionary<string, object>
        {
            { "UniqueID", PlayerPrefs.GetInt("UniqueID")},
            { "CompletionTime", time},
            { "IncorrectSubmissions", wrongSubmissionsCount },
            { "AmountOfPinches", SingletonScript.singletonScript.amountOfPinches }, 
            { "AoPHitObject",  SingletonScript.singletonScript.amountOfCorrectPinches }
        });

        pinches.text = "RotationGame_" + SystemInfo.deviceUniqueIdentifier + "\nTime: " + time + "\nIncorrect Submissions: " + 
                       wrongSubmissionsCount + "\nAmount of Pinches: " +
                       SingletonScript.singletonScript.amountOfPinches + "\nAoP Hit Object: " +
                       SingletonScript.singletonScript.amountOfCorrectPinches;
    }
}
